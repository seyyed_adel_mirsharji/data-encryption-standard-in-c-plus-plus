#include <iostream>
#include <iomanip>
#include <ios>
#include <limits>
#include <vector>
#include <string>
#include <sstream>
#include <bits/stdc++.h> 

using namespace std;

void get_Key(string &key);
void create_keys(string key, vector<string> &forty_eight_bit_keys);
void genrate_fifty_six_bit_key(string sixty_four_bit_key_string, string &fifty_six_bit_key);
void generate_forty_eight_bits_keys(vector<string> &forty_eight_bit_keys, string sixty_four_bit_key);
void make_portions(string text, vector<string> &portions, int portion_size);
void convert_to_64bit_binary_string(vector<string> portions, vector<string> &sixty_four_bit_strings);
void encryption(vector<string> forty_eight_bit_keys);
void encrypt(string sixty_four_bit_string, vector<string> &encrypted_message, vector<string> forty_eight_bit_keys, int key_number, int key_operator);
void decryption(vector<string> forty_eight_bit_keys);
void calculate_final_left_and_right_halves(string right_half, string left_half, string &encrypted_message_string, vector<string> forty_eight_bit_keys, int key_number, int key_operator);
string xor_function(string right_half, string key, int bit_size);
string Sboxs_function(string right_half);
void copy_array(int array_output[4][16], int array_input[4][16]);
void convert_64bit_string_to_char(vector<string> sixty_four_bit_strings, vector<string> &decrypted_message);


int main(){

     string KEY = "";
     vector<string> forty_eight_bit_keys;

     bool end = true;
     while (end){

          int choice = 0;
          int result = 0;

          cout << endl << "Please choose your operation:" << endl;
          cout << "1-Enter new Key" << endl;
          cout << "2-Show current Key" << endl;
          cout << "3-Encryption" << endl;
          cout << "4-Decryption" << endl;
          cout << "5-Exit program" << endl;
          cout << endl << "Your choice = ";

          while (result == 0){

               while(!(cin >> choice)){

                    cout << endl << "Please enter a valid value (number)!" << endl << "your choice: ";
                    cin.clear();
                    cin.ignore(numeric_limits<streamsize>::max(), '\n');
               }
               
               if(choice < 1 || choice > 5){

                    cout << endl << "Please enter number either 0 ro 1!" << endl << "your choice: " ;
               }
               else{

                    result = 1;
               }

          }

          switch (choice)
          {
               case 1:
                    if(KEY.empty())
                         get_Key(KEY);
                    else{

                         get_Key(KEY);
                         forty_eight_bit_keys.clear();
                         create_keys(KEY,forty_eight_bit_keys);
                    }
                    break;

               case 2:
                    if(KEY.empty())
                         cout << endl << "**********ERROR: No key entered... Please first a key!**********" <<endl;
                    else
                         cout << endl << "Current Key is = " << KEY << endl;

                    break;

               case 3:
                    if(KEY.empty())
                         cout << endl << "**********ERROR: No key entered... Please first a key!**********" <<endl;
                    
                    else{
                         if (forty_eight_bit_keys.empty())
                              create_keys(KEY,forty_eight_bit_keys);

                         encryption(forty_eight_bit_keys);
                    }
                    break;
               
               case 4:
                    if(KEY.empty())
                         cout << endl << "**********ERROR: No key entered... Please first a key!**********" <<endl;

                    else{
                         if (forty_eight_bit_keys.empty())
                              create_keys(KEY,forty_eight_bit_keys);

                         decryption(forty_eight_bit_keys);
                    }
                    break;

               case 5:
                    cout << endl << "Program closed successfully" << endl;
                    return 0;
                    break;
               
               default:
                    break;
          }

     }
}

void get_Key(string &key){

     cout << endl << "Key must be 8 characters";
     cout << endl << "Please enter new key = " << endl;
     cin >> key;
     while(key.size() != 8){

          cout << endl << "Please enter a key whit 8 characters!" << endl;
          cout << "Please enter new key = " << endl;
          cin >> key;
     } 
     cout << "**********New key successfully added!**********" << endl;
}

void create_keys(string key, vector<string> &forty_eight_bit_keys){

     vector<string> key_portions;
     make_portions(key,key_portions,8);

     vector<string> sixty_four_bit_key;
     convert_to_64bit_binary_string(key_portions,sixty_four_bit_key);
     string sixty_four_bit_key_string = "";
     for(int i = 0; i < sixty_four_bit_key.size() ; ++i){
          sixty_four_bit_key_string = sixty_four_bit_key_string + sixty_four_bit_key[i];
     }

     string fifty_six_bit_key = "";
     genrate_fifty_six_bit_key(sixty_four_bit_key_string,fifty_six_bit_key);
     generate_forty_eight_bits_keys(forty_eight_bit_keys,fifty_six_bit_key);
}

void genrate_fifty_six_bit_key(string sixty_four_bit_key_string, string &fifty_six_bit_key){

     int permutation_choice_one[56] = 
     { 57, 49, 41, 33, 25, 17, 9,
       1, 58, 50, 42, 34, 26,  18,
       10, 2, 59, 51, 43, 35,  27,
       19, 11, 3, 60, 52, 44,  36,
       63, 55, 47, 39, 31, 23, 15,
       7, 62, 54, 46, 38, 30,  22,
       14, 6, 61, 53, 45, 37,  29,
       21, 13, 5, 28, 20, 12,  4  };

     for(int i = 0 ; i < 56 ; ++i){

          int index = permutation_choice_one[i] - 1;
          fifty_six_bit_key = fifty_six_bit_key + sixty_four_bit_key_string[index];
     }
}

void generate_forty_eight_bits_keys(vector<string> &forty_eight_bit_keys, string fifty_six_bit_key){

     string left_half,right_half;
     left_half = fifty_six_bit_key.substr(0,28);
     right_half = fifty_six_bit_key.substr(28);

     int schedule_of_left_shifts[16] = {1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1};
     char temp;
     vector<string> fifty_six_bit_keys;

     for(int i = 0 ; i < 16 ; ++i){
          for(int j = 0 ; j < schedule_of_left_shifts[i] ; ++j){

               temp = left_half[0];
               for(int k = 0 ; k < 27 ; ++k){

                    temp = left_half[k];
                    left_half[k] = left_half[k+1];
                    left_half[k+1] = temp;
               }
               left_half[27] = temp;

               temp = right_half[0];
               for(int k = 0 ; k < 27 ; ++k){

                    temp = right_half[k];
                    right_half[k] = right_half[k+1];
                    right_half[k+1] = temp;
               }
               right_half[27] = temp;
          }
          
          string str = "";
          str = str + left_half;
          str = str + right_half;
          fifty_six_bit_keys.push_back(str);
     }
     
     int permutation_choice_two[48] = 
     {14, 17, 11, 24, 1, 5, 3, 28,
      15, 6, 21, 10, 23, 19, 12, 4,
      26, 8, 16, 7, 27, 20, 13, 2,
      41, 52, 31, 37, 47, 55, 30, 40,
      51, 45, 33, 48, 44, 49, 39, 56,
      34, 53, 46, 42, 50, 36, 29, 32};

     for(int i = 0 ; i < 16 ; ++i){

          string str = "";
          for(int j = 0 ; j < 48 ; ++j){

               int index = permutation_choice_two[j] - 1;
               str = str + fifty_six_bit_keys[i][index];
          }
          forty_eight_bit_keys.push_back(str);
     }
}

void make_portions(string text, vector<string> &portions, int portion_size){

     string portion = "";
     int quotient,remainder;
     int string_lenght = text.length();
     int counter = 0;

     if(string_lenght > portion_size){

          quotient = string_lenght / portion_size;
          remainder = string_lenght % portion_size;

          for(int i = 0; i < quotient ; ++i){

               portion = text.substr(counter,portion_size);
               counter = counter + portion_size;
               portions.push_back(portion);
          }

          if(remainder != 0){

               portion = text.substr(counter,remainder);
               portions.push_back(portion);
          }
     }
     else{

          portions.push_back(text);
     }
}

void convert_to_64bit_binary_string(vector<string> portions, vector<string> &sixty_four_bit_strings){

     stringstream str;
     int integer_number;
     int binary_base_number[4];
     int temp_array[4];
     stringstream ss;
     string sixty_four_bit_string = "";
     

     for(int i = 0 ; i < portions.size() ; ++i){
          sixty_four_bit_string = "";
          
          for (int j = 0; j < portions[i].length() ; ++j)
               str << hex << uppercase << (int)portions[i][j];

          string hex_string = str.str();
          str.str("");
          str.clear();

          for(int j = 0 ; j < hex_string.size() ; ++j){

               str << hex_string[j];
               str >> integer_number;

               for(int k = 0 ; k < 4 ; ++k){

                    temp_array[k] = integer_number % 2;
                    integer_number = integer_number / 2;
               }

               for(int k = 3 , l = 0 ; k >= 0 ; --k , ++l){

                    binary_base_number[l] = temp_array[k];
               }


               for(int k = 0 ; k < 4 ; ++k)
                    ss << binary_base_number[k];

               string binary_string = ss.str();
               sixty_four_bit_string = sixty_four_bit_string + binary_string;
               str.str("");
               str.clear();
               ss.str("");
               ss.clear();
          }

          int nz = (16 - hex_string.length()) * 4;
          for(int k = 0 ; k < nz ; ++k)
               sixty_four_bit_string += '0';

          sixty_four_bit_strings.push_back(sixty_four_bit_string);
     }
}

void encryption(vector<string> forty_eight_bit_keys){
     
     string text;
     cout << endl << "Please enter text you want to encrypt:" << endl;
     cin.clear();
     cin.ignore(numeric_limits<streamsize>::max(), '\n');
     getline(cin,text);

     vector<string> portions;
     make_portions(text,portions,8);

     vector<string> sixty_four_bit_strings;
     convert_to_64bit_binary_string(portions,sixty_four_bit_strings);

     vector<string> encrypted_messages;
     for(int i = 0 ; i < sixty_four_bit_strings.size() ; ++i)
          encrypt(sixty_four_bit_strings[i],encrypted_messages,forty_eight_bit_keys,0,1);

     cout << endl << "Encrypted message using DES algorithm is:" << endl;
     for(int i = 0 ; i < encrypted_messages.size() ; ++i)
          cout << encrypted_messages[i];

     cout << endl << endl;
}

void encrypt(string sixty_four_bit_string, vector<string> &encrypted_messages, vector<string> forty_eight_bit_keys, int key_number, int key_operator){

     string encrypted_message_string = "";

     int initial_permutation[64] = 
     {58, 50, 42, 34, 26, 18, 10, 2,
      60, 52, 44, 36, 28, 20, 12, 4,
      62, 54, 46, 38, 30, 22, 14, 6,
      64, 56, 48, 40, 32, 24, 16, 8,
      57, 49, 41, 33, 25, 17, 9,  1,
      59, 51, 43, 35, 27, 19, 11, 3,
      61, 53, 45, 37, 29, 21, 13, 5,
      63, 55, 47, 39, 31, 23, 15, 7};

     string str = "";
     for(int i = 0 ; i < 64 ; ++i){

          int index = initial_permutation[i] - 1;
          str = str + sixty_four_bit_string[index];
     }

     string left_half = str.substr(0,32);
     string right_half = str.substr(32);
     calculate_final_left_and_right_halves(right_half,left_half,encrypted_message_string,forty_eight_bit_keys,key_number,key_operator);

     int inverse_permutation[64] = 
     {40, 8, 48, 16, 56, 24, 64, 32,
      39, 7, 47, 15, 55, 23, 63, 31,
      38, 6, 46, 14, 54, 22, 62, 30,
      37, 5, 45, 13, 53, 21, 61, 29,
      36, 4, 44, 12, 52, 20, 60, 28,
      35, 3, 43, 11, 51, 19, 59, 27,
      34, 2, 42, 10, 50, 18, 58, 26,
      33, 1, 41, 9, 49, 17, 57, 25};


     str = "";
     for(int i = 0 ; i < 64 ; ++i){

          int index = inverse_permutation[i] - 1;
          str = str + encrypted_message_string[index];
     }

     encrypted_messages.push_back(str);
}

void calculate_final_left_and_right_halves(string right_half, string left_half, string &encrypted_message_string,vector<string> forty_eight_bit_keys, int key_number, int key_operator){

     for(int i = 0 ; i < 16 ; ++i){

          string left_half_to_xor_whit_right_half = left_half;

          left_half = right_half;

          int expansion_permutation[48] =
          {32, 1, 2, 3, 4, 5,
          4, 5, 6, 7, 8, 9,
          8, 9, 10, 11, 12, 13,
          12, 13, 14, 15, 16, 17,
          16, 17, 18, 19, 20, 21,
          20, 21, 22, 23, 24, 25,
          24, 25, 26, 27, 28, 29,
          28, 29, 30, 31, 32, 1};

          string str = "";
          for(int j = 0 ; j < 48 ; ++j){

               int index = expansion_permutation[j] - 1;
               str = str + right_half[index];
          }

          right_half = str;
          str = xor_function(right_half,forty_eight_bit_keys[key_number],48);
          right_half = str;
          str = Sboxs_function(right_half);
          right_half = str;
          str = xor_function(right_half,left_half_to_xor_whit_right_half,32);
          right_half = str;

          key_number = key_number + key_operator;
     }   

     encrypted_message_string = encrypted_message_string + right_half;
     encrypted_message_string = encrypted_message_string + left_half;
}

string xor_function(string right_half, string key, int bit_size){

     string str = "";
     for(int i = 0 ; i < bit_size ; ++i){

          char temp;

          if(right_half[i] == '0' && key[i] == '0')
               temp = '0';

          if(right_half[i] == '1' && key[i] == '1')
               temp = '0';

          if(right_half[i] == '1' && key[i] == '0')
               temp = '1';

          if(right_half[i] == '0' && key[i] == '1')
               temp = '1';

          str = str + temp;
     }

     return str;
}

string Sboxs_function(string right_half){

     int SBOX_1[4][16] = 
     {14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7,
      0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8,
      4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0,
      15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13};

     int SBOX_2[4][16] = 
     {15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10,
      3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5,
      0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15,
      13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9};

     int SBOX_3[4][16] = 
     {10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8,
      13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1,
      13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7,
      1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12};

     int SBOX_4[4][16] =
     {7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15,
      13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9,
      10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4,
      3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14};

     int SBOX_5[4][16] =
     {2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9,
      14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6,
      4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14,
      11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3};

     int SBOX_6[4][16] =
     {12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11,
      10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8,
      9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6,
      4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13};

     int SBOX_7[4][16] =
     {4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1,
      13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6,
      1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2,
      6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12};

     int SBOX_8[4][16] =
     {13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7,
      1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2,
      7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8,
      2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11};

     string part1,part2,part3,part4,part5,part6,part7,part8;
     part1 = right_half.substr(0,6);
     part2 = right_half.substr(6,6);
     part3 = right_half.substr(12,6);
     part4 = right_half.substr(18,6);
     part5 = right_half.substr(24,6);
     part6 = right_half.substr(30,6);
     part7 = right_half.substr(36,6);
     part8 = right_half.substr(42,6);

     vector<string> six_bit_parts;
     six_bit_parts.push_back(part1);
     six_bit_parts.push_back(part2);
     six_bit_parts.push_back(part3);
     six_bit_parts.push_back(part4);
     six_bit_parts.push_back(part5);
     six_bit_parts.push_back(part6);
     six_bit_parts.push_back(part7);
     six_bit_parts.push_back(part8);

     int array[4][16];
     int six_bit_integer[6] = {0};
     int row,col;
     right_half = "";

     for(int i = 0 ; i < 8 ; ++i){

          row = 0;
          col = 0;
          string str = six_bit_parts[i];

          for(int j = 0 ; j < 6 ; ++j)
               six_bit_integer[j] = 0;

          for(int j = 0 ; j < 6 ; ++j)
               six_bit_integer[j] = six_bit_integer[j] * 10 + (str[j] - 48);

          row = row + (six_bit_integer[0] * 2);
          row = row + six_bit_integer[5];

          col = col + (six_bit_integer[1] * 8);
          col = col + (six_bit_integer[2] * 4);
          col = col + (six_bit_integer[3] * 2);
          col = col + six_bit_integer[4]; 

          switch (i)
          {
          case 0:
               copy_array(array,SBOX_1);
               break;
          
          case 1:
               copy_array(array,SBOX_2);
               break;
          
          case 2:
               copy_array(array,SBOX_3);
               break;

          case 3:
               copy_array(array,SBOX_4);
               break;

          case 4:
               copy_array(array,SBOX_5);
               break;

          case 5:
               copy_array(array,SBOX_6);
               break;
          
          case 6:
               copy_array(array,SBOX_7);
               break;

          case 7:
               copy_array(array,SBOX_8);
               break;
          
          default:
               break;
          }

          int sbox_result = array[row][col];
          string binary_result = "";
          string four_bit_binary_result = "";
          while(sbox_result > 0){

               binary_result = binary_result + to_string(sbox_result % 2);
               sbox_result = sbox_result / 2;
          }
          
          int number_of_zeros_to_be_added_to_first_of_four_bit_binary = 4 - binary_result.length();
          for(int j = 0 ; j < number_of_zeros_to_be_added_to_first_of_four_bit_binary ; ++j)
               four_bit_binary_result = four_bit_binary_result + '0';

          reverse(binary_result.begin(), binary_result.end());
          four_bit_binary_result = four_bit_binary_result + binary_result;

          right_half = right_half + four_bit_binary_result;
     }

     int Sbox_permutation[32] = 
     {16, 7, 20, 21, 29, 12, 28, 17,
      1, 15, 23, 26, 5, 18, 31, 10,
      2, 8, 24, 14, 32, 27, 3, 9,
      19, 13, 30, 6, 22, 11, 4, 25};

     string str = "";
     for(int i = 0 ; i < 32 ; ++i){

          int index = Sbox_permutation[i] - 1;
          str =  str + right_half[index];
     } 
     
     return str;
}

void copy_array(int array_output[4][16], int array_input[4][16]){

     for(int i = 0 ; i < 4 ; ++i){
          for(int j = 0 ; j < 16 ; ++j)
               array_output[i][j] = array_input[i][j];
     }
}

void decryption(vector<string> forty_eight_bit_keys){

     string text;
     cout << endl << "Please enter encrypted text to decrypt:" << endl;
     cin.clear();
     cin.ignore(numeric_limits<streamsize>::max(), '\n');
     getline(cin,text);

     vector<string> portions;
     make_portions(text,portions,64);

     vector<string> decrypted_messages_sixty_four_bit;
     for(int i = 0 ; i < portions.size() ; ++i)
          encrypt(portions[i],decrypted_messages_sixty_four_bit,forty_eight_bit_keys,15,-1);

     vector<string> decrypted_messages;
     convert_64bit_string_to_char(decrypted_messages_sixty_four_bit,decrypted_messages);

     cout << endl << "Decrypted message using DES algorithm is:" << endl;
     for(int i = 0 ; i < decrypted_messages.size() ; ++i)
          cout << decrypted_messages[i];

     cout << endl << endl << endl;
}

void convert_64bit_string_to_char(vector<string> sixty_four_bit_strings, vector<string> &decrypted_message){

     for(int i = 0 ; i < sixty_four_bit_strings.size() ; ++i){

          vector<string> portions;
          make_portions(sixty_four_bit_strings[i],portions,8);
          string message = "";

          for(int j = 0 ; j < portions.size() ; ++j){
              
               string str = portions[j];
               int four_bit_binary[8] = {0};
               int decimal_ascii = 0;
               for(int k = 0 ; k < 8 ; ++k)
                    four_bit_binary[k] = four_bit_binary[k] * 10 + (str[k] - 48);

               int power_of_two = 128;
               for(int k = 0 ; k < 8 ;++k){
                    decimal_ascii = decimal_ascii + four_bit_binary[k] * power_of_two;
                    power_of_two = power_of_two / 2;
               }

               char charater = decimal_ascii;
               message = message + charater; 
          }

          decrypted_message.push_back(message);
     }
}

//example = ABCDEFG ABCDEFG HIJ



